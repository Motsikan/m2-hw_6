/* Теоретичні питання ====================================
----------------------------------------------------------
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування?
--
Якщо потрібно у строці всередині написати рядок з лапками, то використовується екранування
за допомогою (\) оберненого сплешуь(backslash), який ставиться перед лапками .
Приклад: 
let String = "Фільм \"Друзі\" номіновано на \"Оскар\""; // Фільм "Друзі" номіновано на "Оскар"

----------------------------------------------------------
2. Які засоби оголошення функцій ви знаєте?
--
* function declaration; 
* function expression;
* arrow function;
* methode (методом);

----------------------------------------------------------
3. Що таке hoisting, як він працює для змінних та функцій?
--
Hoisting (підняття) - це переміщення в пам'ять оголошень змінних, функцій та ін. 
до виконання коду, тобто, пересуваючись вгору своєї області видимості, дають 
можливість ініціалізувати та використовувати змінні, функції до їх оголошення.
*/
// Завдання: ===================================================

/*Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
*  Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
1. При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
2. Створити метод getAge() який повертатиме скільки користувачеві років.
3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
*  Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

Література:
Дата і час
----------------------------------------------------------------
*/
let createNewUser = function() {  
         
    let userName = prompt("Enter your firstName: ");
    let userSurname = prompt("Enter your lastName: ");
    let myDay = new Date(prompt("Enter your date of birthday: ", "dd.mm.yyyy"));   
    // console.log(myDay);

    let newUser = {
        firstName: userName,
        lastName: userSurname,
        birthday: myDay,
        
  
            
        getLogin() {
            let firstLet = this.firstName[0].toLowerCase();
            let login = firstLet + this.lastName.toLowerCase();

        return login; 
        },

        getAge() {
            let yearNow = new Date().getFullYear();
            let yearOfBirthday = myDay.getFullYear();

        return yearNow - yearOfBirthday;
        },

        getPassword() {
            let yearOfBirthday = myDay.getFullYear();
            let firstLetter = this.firstName[0].toUpperCase();
            let login2 = firstLetter + this.lastName.toLowerCase() + yearOfBirthday;

        return login2;
        
        },
    }
    return newUser;        
}   

let createUser = createNewUser();

console.log(createUser);
console.log(createUser.getAge());
console.log(createUser.getPassword());
